package hoomsun.com.lc.dialogmanagersimple;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;

/**
 * Created by hoomsun on 2018/5/25.
 */
@Route(path = Path.DlaiogActivity01)
public class DialogActivity01 extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog_01);
        ARouter.getInstance().inject(this);
    }
}
