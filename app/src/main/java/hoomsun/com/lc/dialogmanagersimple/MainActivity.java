package hoomsun.com.lc.dialogmanagersimple;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;

import hoomsun.com.lc.dialogmanager.ActivityBean;
import hoomsun.com.lc.dialogmanager.Config;
import hoomsun.com.lc.dialogmanager.DialogManager;

public class MainActivity extends AppCompatActivity {

    TextView btn_start;
    DialogManager dialogManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ARouter.init(getApplication());
        ARouter.getInstance().inject(this);
        dialogManager =DialogManager.getInstance();
        dialogManager.setDialogType(Config.ACTIVITY);
        initActivityDialog();
        dialogManager.show();
//        initDialog();
//        dialogManager.addValue(new ActivityBean("",new Bundle(),this),10);
//        btn_start=findViewById(R.id.btn_start);
//        btn_start.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                initDialog();
//                initActivityDialog();
//                dialogManager.notifyDialogShow();
//            }
//        });

    }

    private void initActivityDialog() {
        dialogManager.addValue(new ActivityBean(Path.DlaiogActivity01,null,MainActivity.this),1);
        dialogManager.notifyDialogShow();
    }

    private void initDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("title")
                .setMessage("message")
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                })
                .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialogManager.showNext();
                    }
                });
        AlertDialog alertDialog=builder.create();
        dialogManager.addValue(alertDialog,10);

        AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
        builder1.setTitle("title")
                .setMessage("message222")
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                })
                .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialogManager.showNext();
                    }
                });
        AlertDialog alertDialog1=builder1.create();
        dialogManager.addValue(alertDialog1,9);
    }


}
