package hoomsun.com.lc.dialogmanager;

import android.app.Dialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;
import android.util.Pair;

import com.alibaba.android.arouter.launcher.ARouter;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by hoomsun on 2018/5/23.
 */

public class DialogManager {
    private static final String TAG = DialogManager.class.getSimpleName();
    private volatile static DialogManager dialogManager;
    private List<Pair<Dialog, Integer>> dialogList = new ArrayList<>();
    private List<Pair<ActivityBean, Integer>> activityList = new ArrayList<>();
    private List<Pair<Object, Integer>> bothList = new ArrayList<>();
    private static int DialogType = Config.DIALOG;

    private DialogManager() {
    }

    public static DialogManager getInstance() {
        if (dialogManager == null) {
            synchronized (DialogManager.class) {
                if (dialogManager == null) {
                    dialogManager = new DialogManager();
                }
            }
        }
        return dialogManager;
    }

    public void setDialogType(int type) {
        DialogType = type;
    }

    /**
     * 添加Dialog对象
     *
     * @param dialog
     * @param priority 展示优先级
     */
    public void addValue(@NonNull Dialog dialog, int priority) {
        if (priority >= 0) {
            Pair pair = new Pair<>(dialog, priority);
            if (DialogType == Config.DIALOG) {
                if (!dialogList.contains(pair)) {
                    dialogList.add(pair);
                }
            }
            if (DialogType == Config.BOTH) {
                if (!bothList.contains(pair)) {
                    bothList.add(pair);
                }
            }
        }
    }

    public void addValue(@NonNull ActivityBean activity, int priority) {
        if (priority >= 0) {
            Pair pair = new Pair<>(activity, priority);
            if (DialogType == Config.ACTIVITY) {
                if (!activityList.contains(pair)) {
                    activityList.add(pair);
                }
            }
            if (DialogType == Config.BOTH) {
                if (!bothList.contains(pair)) {
                    bothList.add(pair);
                }
            }
        }
    }

    public void show() {
        //优先级的查询
        //1、整体排序，然后遍历展示
        //2、查找优先级最高的展示
        switch (DialogType) {
            case 0: {
                if (dialogList == null || dialogList.size() == 0) {
                    Log.e(TAG, "没有可以展示的dialog");
                    return;
                }
                int position = checkHighestPriorty(dialogList);
                dialogList.get(position).first.show();
                //展示成功以後删除集合内部的dialog
                dialogList.remove(position);
            }
            break;
            case 1: {
                if (activityList == null || activityList.size() == 0) {
                    Log.e(TAG, "没有可以展示的activity");
                    return;
                }
                //activity的对应操作
                int position = checkHighestActivityPriorty(activityList);
                start(activityList.get(position).first);
                //展示成功以後删除集合内部的dialog
                activityList.remove(position);
            }
            break;
            case 2:
                //both的对应操作
                break;
        }
    }

    private void start(ActivityBean first) {
        Log.e(TAG, "start" + first.getTargetPath());
//        Intent intent=new Intent();
//        intent.setClass();
        //完成页面跳转
//        ARouter.getInstance().build(first.getTargetPath()).navigation();
    }

    public void showNext() {
        show();
    }

    public void notifyDialogShow() {
        show();
    }

    private int checkHighestPriorty(List<Pair<Dialog, Integer>> dialogList) {
        for (int i = 0; i < dialogList.size(); i++) {
            if (i - 1 >= 0) {
                if (dialogList.get(i).second >= dialogList.get(i - 1).second) {
                    return i;
                }
            }
        }
        return 0;
    }

    private int checkHighestActivityPriorty(List<Pair<ActivityBean, Integer>> dialogList) {
        for (int i = 0; i < dialogList.size(); i++) {
            if (i - 1 >= 0) {
                if (dialogList.get(i).second >= dialogList.get(i - 1).second) {
                    return i;
                }
            }
        }
        return 0;
    }

    public void onDestory() {
        dialogManager = null;
    }

}
