package hoomsun.com.lc.dialogmanager;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by hoomsun on 2018/5/23.
 */

public class ActivityBean {
    private String targetPath;
    private Bundle bundle;
    private Activity activity;

    public ActivityBean(String targetPath, Bundle bundle, Activity activity) {
        this.targetPath = targetPath;
        this.bundle = bundle;
        this.activity = activity;
    }

    public String getTargetPath() {
        return targetPath;
    }

    public void setTargetPath(String targetPath) {
        this.targetPath = targetPath;
    }

    public Bundle getBundle() {
        return bundle;
    }

    public void setBundle(Bundle bundle) {
        this.bundle = bundle;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }
}
